﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        ParkingService parkingService;

        public VehiclesController(ParkingService parkingServ)
        {
            parkingService = parkingServ;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            return Ok(parkingService.GetVehicles());
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult GetID(string id)
        {
            if(parkingService.FindVehicleById(id) == null)
                return BadRequest("Bad Request");
            try
            {
                return Ok(parkingService.FindVehicleById(id));
            }
            catch
            {
                return NotFound("Not found");
            }

        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult Post([FromBody] Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return Created("api/vehicles", vehicle);
            }
            catch
            {
                return BadRequest(new Exception("Invalid body."));
            }
            
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            // знаю, що логіку в контролерах не варто писати
            if (regex.IsMatch(id))
                return BadRequest("Хибний формат ID.");

            if (parkingService.FindVehicleById(id) == null)
                return NotFound();

            return NoContent();

        }
    }
}

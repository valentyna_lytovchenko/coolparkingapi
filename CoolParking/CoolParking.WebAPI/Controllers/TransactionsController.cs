﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        ParkingService parkingService;

        public TransactionsController(ParkingService parkingServ)
        {
            parkingService = parkingServ;
        }

        // GET api/transactions/all 
        [HttpGet("all")]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(parkingService.ReadFromLog());
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<string> GetLast()
        {
            try
            {
                return Ok(parkingService.GetLastParkingTransactions());
            }
            catch
            {
                return NotFound("Лог-файл не знайдено.");
            }
            
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult Put(string id, decimal value)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (regex.IsMatch(id))
                return BadRequest("Хибний формат ID.");

            if (parkingService.FindVehicleById(id) == null)
                return NotFound();

            parkingService.TopUpVehicle(id, value);
            return Ok();
        }

    }
}

﻿using System;
using System.Net.Http;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using static System.Console;

namespace Menu
{
	class Program
	{
		private static readonly HttpClient client = new HttpClient();
		ParkingService parkingServise;

		enum optionsEnum
		{
			BalanceShow,
			TransactionsEarned,
			FreeSpace,
			Transactions,
			TransactionsHistory,
			CurrentVehicles,
			AddVehicle,
			RemoveVehicle,
			TopUpVehicle
		}

		static decimal SumCharges(TransactionInfo[] transactions)
		{
			decimal sum = 0;
			if (transactions != null)
				Array.ForEach(transactions, info => sum += info.Sum);
			return sum;
		}

		static string TransactionsCat(TransactionInfo[] transactions)
		{
			string res = "";
			if (transactions != null)
				Array.ForEach(transactions, info => res += info.ToString() + '\n');
			else
				res = "No transactions";
			return res;
		}

		static void MenuShow()
		{
			int currentOption = 0;
			var indent = new string(' ', 13);
			// 9 options
			while (true)
			{
				WriteLine(indent + "Show parking balance");
				WriteLine(indent + "Show current money earned");
				WriteLine(indent + "Show free space on the parking");
				WriteLine(indent + "Show current transactions");
				WriteLine(indent + "Show transactions history");
				WriteLine(indent + "Show vehicles on the parking");
				WriteLine(indent + "Add vehicle to the parking");
				WriteLine(indent + "Remove vehicle from the parking");
				WriteLine(indent + "Top up a vehicle");
				SetCursorPosition(5, currentOption);
				Write("-->");

				var c = ReadKey();
				Clear();
				switch (c.Key)
				{
					case ConsoleKey.DownArrow:
						currentOption += currentOption == 9 ? 0 : 1;
						break;
					case ConsoleKey.UpArrow:
						currentOption -= currentOption == 0 ? 0 : 1;
						break;
					case ConsoleKey.Enter:
						switch ((optionsEnum)currentOption)
						{
							case optionsEnum.BalanceShow:
								WriteLine("\n\n" + indent + "Current balance: " + client.Send(new HttpRequestMessage(HttpMethod.Get, "localhost:44327/api/parking/balance")));
								ReadKey();
								break;
							case optionsEnum.TransactionsEarned:
								WriteLine("\n\n" + indent + "Money earned: " + SumCharges(parkingService.GetLastParkingTransactions()));
								ReadKey();
								break;
							case optionsEnum.FreeSpace:
								WriteLine("\n\n" + indent + "Free spaces: " + parkingService.GetFreePlaces());
								ReadKey();
								break;
							case optionsEnum.Transactions:
								WriteLine("Current transactions: ");
								WriteLine(TransactionsCat(parkingService.GetLastParkingTransactions()));
								ReadKey();
								break;
							case optionsEnum.TransactionsHistory:
								WriteLine("Transactions log: ");
								try { WriteLine(parkingService.ReadFromLog()); }
								catch (Exception) { WriteLine("No log, because no transactions"); }
								ReadKey();
								break;
							case optionsEnum.CurrentVehicles:
								WriteLine("Current vehicles: ");
								foreach (var vehicle in parkingService.GetVehicles())
									WriteLine(vehicle);
								ReadKey();
								break;
							case optionsEnum.AddVehicle:
								Write("ID (WW-DDDD-WW format, or \"auto\" to autofill): ");
								var idAdd = ReadLine();
								if (idAdd.Trim() == "auto")
									idAdd = Vehicle.GenerateRandomRegistrationPlateNumber();
								Write("Balance: ");
								var balanceAdd = decimal.Parse(ReadLine());
								Write($"Type of vehicle 0 - {Settings.Tariff.Keys.Count - 1} ( ");
								foreach (var typeKey in Settings.VehicleTypeString.Keys)
									Write(Settings.VehicleTypeString[typeKey] + " | ");
								Write(" ): ");
								var typeAdd = int.Parse(ReadLine());
								parkingService.AddVehicle(
									new Vehicle(idAdd, (VehicleType)typeAdd, balanceAdd)
									);
								break;
							case optionsEnum.RemoveVehicle:
								WriteLine("Current vehicles: ");
								foreach (var vehicle in parkingService.GetVehicles())
									WriteLine(vehicle);
								Write("ID: ");
								var idRm = ReadLine();
								if (idRm == "")
									break;
								parkingService.RemoveVehicle(idRm);
								break;
							case optionsEnum.TopUpVehicle:
								Write("Id to top up: ");
								var idTopUp = ReadLine();
								Write("Amount to top up: ");
								var amount = decimal.Parse(ReadLine());
								parkingService.TopUpVehicle(idTopUp, amount);
								break;
						}
						Console.Clear();
						break;
					default:
						break;
				}
			}
		}

		static void Main(string[] args)
		{			
			MenuShow();

			parkingServise = new ParkingService(new TimerService(Settings.PaymentPeriodSeconds * 1000),
							new TimerService(Settings.LogingPeriodSeconds * 1000),
							new LogService(Settings.FilePath));

			// client.Send(new HttpRequestMessage(HttpMethod.Get, "localhost:44327/api/..."));
			// client.Send(new HttpRequestMessage(HttpMethod.Post, "localhost:44327"));

		}
	}
}
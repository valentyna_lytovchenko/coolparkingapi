﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; } = Settings.StartBalance;

        public List<Vehicle> vehicles = new List<Vehicle>(Settings.ParkingCapacity); 

        private Parking() { }

        private static Parking _instance;

        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }
    }
}
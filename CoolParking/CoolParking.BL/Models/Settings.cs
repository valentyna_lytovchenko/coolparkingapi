﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal StartBalance = 0;
        public const int ParkingCapacity = 10;
        public const int PaymentPeriodSeconds = 5;
        public const int LogingPeriodSeconds = 60;
        public const float Fine = 2.5F;

        public const string FilePath = @"./Transactions.log";

        public static readonly Dictionary<VehicleType, decimal> Tariff = new Dictionary<VehicleType, decimal>(
            new Dictionary<VehicleType, decimal>(4)
            {
                { VehicleType.PassengerCar, 2.0m },
                { VehicleType.Truck, 5.0m },
                { VehicleType.Bus, 3.5m },
                { VehicleType.Motorcycle, 1.0m }
            });

        public static readonly Dictionary<VehicleType, string> VehicleTypeString
            = new Dictionary<VehicleType, string>()
            {
                {VehicleType.Motorcycle, "Motorcycle"},
                {VehicleType.PassengerCar, "PassengerCar"},
                {VehicleType.Bus, "Bus"},
                {VehicleType.Truck, "Truck"}
            };
    }
}



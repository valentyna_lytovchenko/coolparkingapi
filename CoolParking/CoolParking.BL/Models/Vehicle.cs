﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; } 
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            if (Balance < 0)
                throw new ArgumentException();

            this.Balance = Balance;
            SetId(Id);
            this.VehicleType = VehicleType;
        }   

        private void SetId(string Id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (regex.IsMatch(Id))
                this.Id = Id;
            else
                throw new ArgumentException();
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string vehicleNum = "";
            Random rnd = new Random((int)(DateTime.Now.Ticks));

            vehicleNum += GetNRandomChars(rnd, 2);
            vehicleNum += '-';
            for (int i = 0; i < 4; i++)
                vehicleNum += rnd.Next(10).ToString();
            vehicleNum += '-';
            vehicleNum += GetNRandomChars(rnd, 2);
            return vehicleNum;
        }

        private static string GetNRandomChars(Random rnd, int n = 1)
        {
            // English alphabet +1 `cause including Z
            var alphabet = Enumerable.Range('A', 'Z' - 'A' + 1).ToArray();
            string res = "";

            for (int i = 0; i < n; i++)
                res += (char)rnd.Next(alphabet.First(), alphabet.Last());

            return res.ToString();
        }

        public override string ToString() =>
            $"\"{this.Id}\", Balance: {Balance} {Settings.VehicleTypeString[this.VehicleType]}";
    }
}


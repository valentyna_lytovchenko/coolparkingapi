﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed {
            add => _timer.Elapsed += value;
            remove => _timer.Elapsed -= value;
        }

        private Timer _timer;

        public TimerService(double interval, ElapsedEventHandler callback = null)
        {    
            Interval = interval;
            _timer = new Timer(Interval); 

            if(callback != null)
            {
                Elapsed += callback;
                _timer.Elapsed += callback;
            }                        
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }


}